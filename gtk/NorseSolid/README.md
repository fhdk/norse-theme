The themes are compiled from different sources

![](screenshots/nordic.png)

> [Nord-Openbox](https://github.com/the-zero885/Nord-Openbox-theme) created to match the Nord darker

![](screenshots/nordic.png)

> Nordic is a Gtk3.20+ theme created using the awesome [Nord](https://github.com/arcticicestudio/nord) color pallete.


